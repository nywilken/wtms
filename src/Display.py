"""
Google Voice SMS Reading Client
Author: Wilken Rivera <wrivera00 at gmail dot com>
"""
import wx
from SMSPanels import *
import SMSClient

from wx.lib.colourdb import *

class MyFrame(wx.Frame):
    def __init__(self, parent, id=wx.ID_ANY, title="", pos=wx.DefaultPosition,
                 size=(1024,768), style=wx.DEFAULT_FRAME_STYLE):

        wx.Frame.__init__(self, parent, id, title, pos, size, style)

        self._flags = 0
        self.ID_WINDOW_TOP = 100
        self.ID_WINDOW_CONTROL_BAR = 101
        self.ID_WINDOW_SCREEN_PANEL = 102
        self.ID_WINDOW_BOTTOM = 103


        self.Bind(wx.EVT_SIZE, self.OnSize)

        self._pnl = 0

        # will occupy the space not used by the Layout Algorithm
        self.screenPanel = GreetingsPanel(self, -1)


        self.top_window = wx.SashLayoutWindow(self, -1, wx.DefaultPosition,
                                            wx.Size(375, 1080),
                                            wx.NO_BORDER|wx.SW_3DSASH|wx.CLIP_CHILDREN)


        self.top_window.SetDefaultSize(wx.Size(375, 180))
        self.top_window.SetOrientation(wx.LAYOUT_VERTICAL)
        self.top_window.SetAlignment(wx.LAYOUT_LEFT)
        self.top_window.SetExtraBorderSize(0)

        self.topPanel = wx.Panel(self.top_window, -1, (0,0), (375,840),
                                 style=wx.NO_BORDER)

        leftSidebanner = wx.Bitmap("configs/WTMSBackdrop_left.png", wx.BITMAP_TYPE_PNG)
        leftSide = wx.StaticBitmap(self.topPanel, -1, leftSidebanner)


        topSizer = wx.BoxSizer(wx.VERTICAL)
        topSizer.Add(leftSide, 1, wx.EXPAND|wx.ALIGN_BOTTOM)
        self.topPanel.SetSizer(topSizer)
        topSizer.Fit(self.topPanel)


        self.sms_window = wx.SashLayoutWindow(self, -1, wx.DefaultPosition,
                                            wx.Size(1033, 220),
                                            wx.NO_BORDER|wx.SW_3DSASH|wx.CLIP_CHILDREN)

        self.sms_window.SetDefaultSize(wx.Size(1033, 220))
        self.sms_window.SetOrientation(wx.LAYOUT_HORIZONTAL)
        self.sms_window.SetAlignment(wx.LAYOUT_TOP)
        self.sms_window.SetExtraBorderSize(0)

        self.smsPanel = wx.Panel(self.sms_window, -1, (3,0), (1033, 220),
                                 style=wx.NO_BORDER)

        titlebanner = wx.Bitmap("configs/WTMSBackdrop_top.png", wx.BITMAP_TYPE_PNG)
        sms_title = wx.StaticBitmap(self.smsPanel, -1, titlebanner)

        smsSizer = wx.BoxSizer(wx.VERTICAL)
        smsSizer.Add(sms_title, 0, wx.ALIGN_CENTER|wx.RIGHT, 15)
        self.smsPanel.SetSizer(smsSizer)
        smsSizer.Fit(self.smsPanel)
        self.smsPanel.SetBackgroundColour(wx.WHITE)
        self.topPanel.SetBackgroundColour(wx.WHITE)


        # Initialize SMSClient actions
        self.msg_downloader = SMSClient.Downloader()
        self.msg_reader = SMSClient.Reader()

        import thread
        thread.start_new_thread(self.msg_reader.connect, ())
        self.timer = wx.Timer(self)
        self.Bind(wx.EVT_TIMER, self.OnTimer, self.timer)
        self.timer.Start(500)

        self.ShowFullScreen(True, style=wx.FULLSCREEN_ALL)
    def OnSize(self, event):
        wx.LayoutAlgorithm().LayoutWindow(self, self.screenPanel)
        event.Skip()

    def OnTimer(self, event):
        # Start the Client Polling
        self.msg_reader.connect()
        nextmsg = self.msg_reader.readnext()
        if nextmsg:
            self.screenPanel.Destroy()
            self.screenPanel = SMSMessagePanel(self, -1, nextmsg)
        else:
            self.screenPanel.Destroy()
            self.screenPanel = GreetingsPanel(self, -1)

        self.OnSize(event)
        self.screenPanel.Refresh()

        self.msg_downloader.poll()
        self.timer.Start(3000)

class MyApp(wx.App):
    def OnInit(self):
        updateColourDB()
        frame = MyFrame(None, -1, 'MrAndMrsRivera SMSServer', size=(800,600))
        frame.Show(True)
        self.SetTopWindow(frame)
        return True

app = MyApp(0)
app.MainLoop()
