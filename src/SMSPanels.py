"""
Author: Wilken Rivera <wrivera00 at gmail dot com>

Two Panel classes which are used to control the text
being displayed by the SMSClient Applciation.
"""
import wx
from stext import StaticWrapText

class GreetingsPanel(wx.Panel):
    def __init__(self, parent, winid):
        wx.Panel.__init__(self, parent, winid, size=wx.DisplaySize())
        self.parent = parent
        font1 = wx.Font(55, wx.FONTFAMILY_DECORATIVE, wx.NORMAL, wx.NORMAL, False, u'Arial Narrow')
        self.label = wx.StaticText(self, -1, 
                     "Send A Text Message To The Bride and Groom\n\t\t\t### . ### . ####", 
                     style=wx.ALIGN_RIGHT)
        self.label.SetFont(font1)
        self.label.SetForegroundColour(wx.Colour(128, 0, 128))
        self.__do_layout()

    def __do_layout(self):
        self.grid_sizer = wx.BoxSizer(wx.VERTICAL)
        self.grid_sizer.Add(self.label, 1, wx.EXPAND|wx.ALIGN_RIGHT|wx.TOP|wx.RIGHT, 40)

        self.SetSizer(self.grid_sizer)
        self.grid_sizer.Fit(self)
        self.SetBackgroundColour(wx.WHITE)


class SMSMessagePanel(wx.Panel):
    def __init__(self, parent, winid, message):
        wx.Panel.__init__(self, parent, winid, size=wx.DisplaySize())
        self.parent = parent
        font1 = wx.Font(55, wx.FONTFAMILY_DECORATIVE, wx.NORMAL, wx.NORMAL, False, u'Arial Narrow')
        self.label = wx.StaticText(self, winid, message, style=wx.ALIGN_CENTER)
        self.label.SetFont(font1)
        self.label.SetForegroundColour(wx.Colour(128, 0, 128))
        self.__do_layout()

    def __do_layout(self):
        self.grid_sizer = wx.BoxSizer(wx.VERTICAL)
        self.grid_sizer.Add(self.label, 1, wx.EXPAND|wx.ALIGN_LEFT|wx.TOP, 45)

        self.SetSizer(self.grid_sizer)
        self.grid_sizer.Fit(self)
        self.SetBackgroundColour(wx.WHITE)

