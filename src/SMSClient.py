"""
Google Voice SMS Reading Client
Author: Wilken Rivera <wrivera00 at gmail dot com>

This script connects to the Google
Voice server(s) and downloads all unread
SMS messages to a local inbox file.

"""

from googlevoice import Voice,util
import BeautifulSoup
from collections import deque
import sys, os
import ConfigParser
import sched, time
import shutil
import glob

import unittest

class Downloader(object):

    def __init__(self):
        self.inbox = []
        self.voice = Voice()
        self.configure()

    def configure(self):
        """
        Parses confgs/accounts.cfg and set the appropriate vairables.
        """

        config = ConfigParser.SafeConfigParser()
        config.read('./configs/accounts.cfg')

        # Configure Authentication
        self.username = config.get('Auth', 'email')
        self.passwd = config.get('Auth', 'passwd')

        # Setup polling time
        self.poll_int = config.get('Poll', 'interval')

        # Setup unread and read directories
        self.unread_fldr = config.get("FileStructure", "unread")
        self.read_fldr = config.get("FileStructure", "read")
        try:
            # Lets create the directories if they don't exists
            if not os.path.exists(self.read_fldr):
                os.makedirs(self.read_fldr)
        except (OSError, IOError), error:
            # log mesage
            pass


    def connect(self):
        """
        Opens connection to voice.google.com
        using the credentials defined in the
        accounts.cfg file
        """
        self.voice.login(self.username, self.passwd)

    def extractsms(self, htmlsms):
        """
        extractsms  --  extract SMS messages from BeautifulSoup tree of Google Voice SMS HTML.
        Output is a list of dictionaries, one per message.
        """
        msgitems = []
        #   Extract all conversations by searching for a DIV with an ID at top level.
        tree = BeautifulSoup.BeautifulSoup(htmlsms)                 # parse HTML into tree
        conversations = tree.findAll("div",attrs={"id" : True},recursive=False)
        for conversation in conversations :
            #       For each conversation, extract each row, which is one SMS message.
            rows = conversation.findAll(attrs={"class" : "gc-message-sms-row"})
            for row in rows :
                #   For each row, which is one message, extract all the fields.
                msgitem = {"id" : conversation["id"]}               # tag this message with conversation ID
                spans = row.findAll("span",attrs={"class" : True}, recursive=False)
                for span in spans :                                                 # for all spans in row
                    cl = span["class"].replace('gc-message-sms-', '')
                    msgitem[cl] = (" ".join(span.findAll(text=True))).strip()       # put text in dict
                msgitems.append(msgitem)                                    # add msg dictionary to list
        return msgitems

    def save(self, sms):
        """
        Saves a new incoming message to the unread folder
        """
        try:
            unreadmsg_path = os.path.join(self.unread_fldr, sms['from']+"_"+sms['time']+'.txt')
            readmsg_path = os.path.join(self.read_fldr, sms['from']+"_"+sms['time']+'.txt')
            if not os.path.exists(readmsg_path):
                f = file(unreadmsg_path, 'w')
                note = sms['text']


                f.write(note.encode('utf8'))
                f.close()

        except (IOError, OSError), error:
            pass


    def getmessages(self):
        msgs = self.voice.sms().messages
        for sms in self.extractsms(self.voice.sms.html):
		    if sms:
		        self.save(sms)
        for m in msgs:
            m.delete()


    def poll(self):
        sr = sched.scheduler(time.time, time.sleep)
        sr.enter(int(self.poll_int), 1, self.run, ())
        sr.run()
    def run(self):
        self.connect()
        self.getmessages()


class Reader(object):

    def __init__(self):
        self.msg_queue = deque()
        self.configure()

    def configure(self):
        """
        Parses confgs/accounts.cfg and set the appropriate vairables.
        """

        config = ConfigParser.SafeConfigParser()
        config.read('./configs/accounts.cfg')

        # Setup polling time
        self.poll_int = config.get('Poll', 'interval')

        # Setup unread and read directories
        self.unread_fldr = config.get("FileStructure", "unread")
        self.read_fldr = config.get("FileStructure", "read")
        try:
            if not os.path.exists(self.read_fldr):
                os.makedirs(self.read_fldr)
        except (OSError, IOError), error:
            # log mesage
            pass


    def connect(self):
        """
        Establishes a connection to the unread folder on the 
        local disk and begins to queue up unread messages for future display.
        """

        localpath = os.path.join(os.path.dirname(__file__), self.unread_fldr, '*.txt')
        for message in glob.glob(localpath):
            self.msg_queue.append(message)



    def readnext(self):
        """
        Reads a new incoming message from the unread folder.
        Successfully read messages are moved to the read folder.
        """
        try:
            if len(self.msg_queue):
                msg = self.msg_queue.pop()
                if os.path.exists(msg):
                    body = file(msg).read()
                    if body:
                        shutil.move(msg, self.read_fldr)
                        return body
        except (IOError, OSError), error:
            pass


class TestSMSDownloader(unittest.TestCase):

    def setUp(self):
        self.seq = Downloader()
        self.seq2 = Reader()

    def testConigure(self):
        self.assertEqual(self.seq.username, 'wrivera00@gmail.com')

    def testMessages(self):
        self.seq.connect()
        self.seq.getmessages()

        self.assertTrue(os.path.exists('messages/unread'))


    def testMsgRead(self):
        self.seq2.connect()
        locallist = []
        print self.seq2.readnext()
        localpath = os.path.join(os.path.dirname(__file__), self.seq2.read_fldr, '*.txt')
        for message in glob.glob(localpath):
            locallist.append(message)

        self.assertTrue(len(locallist) >= 1)

    def testPolling(self):
        os.system('rm -rf messages/unread/*')
        self.seq.poll()
        time.sleep(15)
        self.assertTrue(os.path.exists('messages/unread'))



if __name__ == '__main__':
    unittest.main()


